'''
Created on 11/09/2021

@author: Herrera
'''

class Aspirante:
    def __init__(self, folio, nombre, primerAp, segundoAp, edad, direccion,
         telefono, correosElectronicos, redesSociales, carrerasInteres,
         escuelaProcedencia, bachillerato):
        self.folio = folio
        self.nombre = nombre
        self.primerAp = primerAp
        self.segundoAp = segundoAp
        self.edad = edad
        self.direccion = direccion
        self.telefono = telefono
        self.correosElectronicos = correosElectronicos
        self.redesSociales = redesSociales
        self.carrerasInteres = carrerasInteres
        self.escuelaProcedencia = escuelaProcedencia
        self.bachillerato = bachillerato
    
    def __str__(self):
        return f"\nFicha de ingreso: {self.folio} \n==================== \nnombre: {self.nombre} \nprimerAp: {self.primerAp} \nsegundoAP: {self.segundoAp} \nedad: {self.edad} \ndireccion: {self.direccion} \ntelefono: {self.telefono} \ncorreosElectronicos: {self.correosElectronicos} \nredesSociales: {self.redesSociales} \ncarrerasInteres: {self.carrerasInteres}  \nescuelaProcedencia: {self.escuelaProcedencia} \nbachillerato: {self.bachillerato} \n"

class RegistroAspirante:
    def __init__(self, vectorAspirante):
        self.vectorAspirante = vectorAspirante
    
    def registroAspirante(self, tamano):
        self.vectorAspirante = [tamano]
    
    def registrarAspirante(self, a):
        self.vectorAspirante.append(a)
    
    def eliminarAspirante(self, folio):
        for i in range(len(self.vectorAspirante)):
            if (folio == self.vectorAspirante[i].folio):
                self.vectorAspirante.pop(i)
                print(">> Aspirante eliminado")
                break
    
    def imprimirAspirantes(self):
        for i in range(len(self.vectorAspirante)):
            print(self.vectorAspirante[i].__str__())
cont = 0
opcion = 0

vectorAspirante = []
ra = RegistroAspirante(vectorAspirante)

while(opcion != 4):
    print("\nElije opcion: ")
    print("1) Agregar usuario")
    print("2) eliminar usuario")
    print("3) Mostrar usuarios")
    print("4) Salir")
    opcion = int(input("Introduce opcion: "))
    
    if (opcion == 1):
        cont += 1
        folio = "0" + str(cont)
        print("\n>> Introduce datos: ")
        nombre = input("nombre: ")
        primerAp = input("Primer apellido: ")
        segundoAp = input("Segundo apellido: ")
        edad = int(input("edad: "))
        direccion = input("direccion: ")
        telefono = input("telefono: ")
        
        numeroCorreos = int(input("Numero de correos electronicos: "))
        correosElectronicos = []
        for i in range(numeroCorreos):
            correosElectronicos.append(input(f"Introduce correo {i+1}: "))
        
        numeroRedes = int(input("Numero de redes: "))
        redesSociales = []
        for i in range(numeroRedes):
            redesSociales.append(input(f"Introduce red {i+1}: "))
        
        numeroCarreras = int(input("Numero de carreras: "))
        carrerasInteres = []
        for i in range(numeroCarreras):
            carrerasInteres.append(input(f"Introduce carrera {i+1}: "))
        
        escuelaProcedencia = input("Escuela de procedencia: ")
        bachillerato = input("Bechillerato: ")
        
        a = Aspirante(folio, nombre, primerAp, segundoAp, edad, direccion, telefono, correosElectronicos, redesSociales, carrerasInteres, escuelaProcedencia, bachillerato)
        
        ra.registrarAspirante(a)
        
    elif (opcion == 2):
        if(cont == 0 or len(ra.vectorAspirante) == 0):
            print(">> no existen aspirantes")
        else: 
            folio = input("\nIntroduce folio del aspirante a eliminar: ")
            ra.eliminarAspirante(folio)
    
    elif (opcion == 3):
        if(cont == 0 or len(ra.vectorAspirante) == 0):
            print(">> no existen aspirantes")
        else: 
            ra.imprimirAspirantes()
    
    elif (opcion == 4):
        print("Saliendo - - - ")
    else:
        print("Opcion erronea")
        
        

        
    